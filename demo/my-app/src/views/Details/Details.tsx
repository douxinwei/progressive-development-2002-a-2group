import React, { useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import "./Details.css"
import { ListItem, RouterDispatch, RouterStore } from '../../utils/types'
import { getStudyList } from "../../api/api"
import { useDispatch, useSelector } from "react-redux"
// import Header from '../Index'
import '../../index.css'
import {
  EyeOutlined,
  ShareAltOutlined
} from '@ant-design/icons';
import Item from 'antd/lib/list/Item'
type Props = {}

function Details({ }: Props) {
  const localtion = useLocation()
  const navigate = useNavigate()
  let DetailsData = localtion.state
  let id = localtion.state.id
  console.log(id);

  console.log(DetailsData);
  const dispatch: RouterDispatch = useDispatch()
  useEffect(() => {
    dispatch(getStudyList())
  }, [dispatch])
  
  const DetailList = useSelector((state: RouterStore) => state.reducer.StudyList)
  console.log(DetailList);

  const times = (stime: any) => {
    const tim = Math.round(
      (new Date().getTime() - new Date(stime.publishAt).getTime()) /
      1000 /
      60 /
      60 /
      24
    )
    return tim
  }

  return (
    <div className='details'>
      <main>
        <div className="details_container">
          <div className="details_container_main">
            <div className="details_container_main_header">
              <span style={{marginLeft:"20px"}} onClick={() => {
                navigate(-1)
              }}>知识小测</span> /{DetailsData.title}

                </div>
            <div className="details_main_header">
              <div className="details_container_main_header_b1">
                <b>{DetailsData.summary}</b>
              </div>
              <div className="details_container_main_right_top">
                <b>其他知识笔记</b>
              </div>
            </div>
            <div className="details_main_container">
              <div className="details_container_main_left">
                <div className="details_container_img">
                  <img src={DetailsData.cover} alt="" />
                </div>
                <b>{DetailsData.title}</b><br />
                <span>{DetailsData.summary}</span>
                <p>{DetailsData.views}次阅读·{DetailsData.createAt}</p>
                <button disabled>开始阅读</button>
                <p style={{ color: "#3b3939" }}>敬请期待</p>
              </div>
              <div className="details_container_main_right">
                <div className="details_container_main_right_bottom">
                  {
                    DetailList?.map((item:any, index:number) => {
                      return item.id !== id ? <div key={index} className="detail_main" onClick={()=>{
                        navigate('/index/Studydetails/' + item.id, {
                          state: item
                        })
                      }}>
                        <header>
                          <b style={{ fontSize: "18px" }}>{item.title}</b>
                          <b style={{ color: "#ccc", marginLeft: "15px" }}>|</b>
                          <span style={{ marginLeft: "15px" }}>{times(item)}天前</span>
                        </header>
                        <main>
                          <div className="details_container_left">
                            <img src={item.cover} alt="" />
                          </div>
                          <div className="details_container_right">
                            <p>{item.summary}</p>
                            <p style={{ textAlign: "center", }}><EyeOutlined /><span>{item.views}</span>
                              <span>·</span><ShareAltOutlined /><span>分享</span></p>
                          </div>
                        </main>
                      </div> : ""
                    })
                  }
                </div>
              </div>

            </div>
          </div>

        </div>
      </main>
    </div>
  )
}

export default Details