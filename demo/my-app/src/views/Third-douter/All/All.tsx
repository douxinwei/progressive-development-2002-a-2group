import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, StoreType } from '../../../store/modules'

import * as api from "../../../api/getData"
import List  from '../../../components/List/List'
type Props = {}

function All({}: Props) {
    const dispatch:AppDispatch = useDispatch()
    // 获取数据 
     useEffect(()=>{
       dispatch(api.get_list())
     },[])
     
     const data = useSelector((state:StoreType)=>state.reducer.data)
    
  return (
    <div className='all'>
        {
            data && <List data={data}></List>
        }
    </div>
  )
}

export default All