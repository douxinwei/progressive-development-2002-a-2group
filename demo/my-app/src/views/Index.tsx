import React, { useCallback, useEffect, useState } from 'react'
import { Outlet, NavLink } from "react-router-dom"
import { Dropdown, Menu, Space } from 'antd';
import "./Index.css"
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, StoreType } from '../store/modules';
import * as api from '../api/HeaderChange' 
import { secRouter } from '../router/routerConfig';
import { RouteItem } from '../types/types';
import moment from '../utils/moment'
import Mask from '../components/Mask/Mask';
import { Affix, Anchor,BackTop  } from "antd"
type Props = {}

function Index({ }: Props) {
  // 根据时间自动换肤
  // Number(moment().format('HH')) > 7  && Number(moment().format('HH')) < 20 ?true:false
  const [skinFlag,setSkin] = useState(true)
  const dispatch:AppDispatch = useDispatch()
  const lan = useSelector((state:StoreType)=>state.reducer)
   
  // 换肤
  const changeSkin = useCallback(()=>{
    let flag = skinFlag
    setSkin(!flag)
},[skinFlag])


  useEffect(()=>{
    if(skinFlag){
      document.documentElement.style.setProperty('--body-color','#E6EAEE')
      document.documentElement.style.setProperty('--words-color','#000')
      document.documentElement.style.setProperty('--page-color','#fff')
      document.documentElement.style.setProperty('--shadow-color','#ccc')
    }else{
      document.documentElement.style.setProperty('--body-color','#282C35')
      document.documentElement.style.setProperty('--words-color','#fff')
      document.documentElement.style.setProperty('--page-color','#363C48')
      document.documentElement.style.setProperty('--shadow-color','#222')
    }
  },[skinFlag])

  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a target="_blank" rel="noopener noreferrer" onClick={()=>dispatch(api.changeLan('zh'))} >
             中文
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a target="_blank" rel="noopener noreferrer" onClick={()=>dispatch(api.changeLan('en'))}>
              English
            </a>
          ),
          
        },
        
      ]}
    />
  );
 
   // 搜索遮罩层
   const [maskFlag,setmaskFlag] = useState(false)
  return (
    <div className='index'>
      
{/* top */}
      <header>
        <div className="top">
        <ul className='left'>
          <li><NavLink to={"/index/article/all"}>
            <img src="https://gimg3.baidu.com/search/src=http%3A%2F%2Fpics7.baidu.com%2Ffeed%2F7dd98d1001e93901ec7a700cd3566bec37d196ca.jpeg%40f_auto%3Ftoken%3De4730bf17f392d00a233b0b4bb85b636&refer=http%3A%2F%2Fwww.baidu.com&app=2021&size=f360,240&n=0&g=0n&q=75&fmt=auto?sec=1668445200&t=4d1ac9ce8c8986bb6e582906899a9fc4" alt="" />
          </NavLink></li>

          {
            secRouter && secRouter.map((item:RouteItem,index)=>{
              return <li  key={index}><NavLink to={index==0?'/index/article/all':item.path}>{lan[lan.language][index]}</NavLink></li>
            })
          }
        </ul>


        <div className="right">
       
       
        {
          skinFlag ? 
          <>
          <Dropdown overlay={menu}>
          <Space>
          <svg  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3065" width="32" height="32"><path d="M219.4 363.4c11.8 35 31.4 65.4 58.7 92.2 23.2-25.2 40.7-56.2 52-92.2H219.4z m722.2-207.1H508.9L484.2 23.9H82.4C38.1 23.9 2 60 2 104.3v683.1c0 44.3 36.1 80.4 80.4 80.4h366.3l-29.4 132.4h522.4c44.3 0 80.4-36.1 80.4-80.4V236.7c-0.1-44.4-36.2-80.4-80.5-80.4zM396.1 562.2c-47.4-17.5-86.5-39.7-118-65.4-33 29.4-74.2 51-122.1 64.4l-16.5-27.3c46.9-12.4 86-30.9 116.9-57.2-31.9-32.5-54.1-70.1-66.5-112.8h-44.8V333H262c-7.2-13.4-16.5-26.3-27.3-38.6l30.9-11.3c10.8 13.9 20.6 30.4 29.4 49.5h111.8v30.9H362c-14.4 44.3-35 81.4-62.3 111.3 30.4 24.2 68.5 44.3 113.3 60.8l-16.9 26.6z m585.7 357c0 22.2-18 40.2-40.2 40.2H469.8l20.1-92.2h150.9l-86-479.6-0.5 2.6-3.6-19.1 1 0.5-31.4-175.2h421.9c22.2 0 40.2 18 40.2 40.2v682.6h-0.6zM655.2 540.1H766v-29.4H655.2V452h118v-29.4H620.7v211.2h157.1v-29.4H655.2v-64.3z m231.3-63.4c-9.3 0-17.5 1.5-25.2 5.7-7.2 3.6-14.4 9.3-20.1 16.5v-18h-33.5v153h33.5v-92.2c1-12.4 5.2-21.6 12.4-28.3 6.2-5.7 13.4-8.8 21.6-8.8 23.2 0 34.5 12.4 34.5 37.6v91.2h33.5V539c1-41.7-18.6-62.3-56.7-62.3z" p-id="3066"></path></svg>
          </Space>
          </Dropdown>
          <svg className='icon' onClick={changeSkin} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5238" width="32" height="32"><path d="M501.48 493.55m-233.03 0a233.03 233.03 0 1 0 466.06 0 233.03 233.03 0 1 0-466.06 0Z" fill="#F9C626" p-id="5239"></path><path d="M501.52 185.35H478.9c-8.28 0-15-6.72-15-15V87.59c0-8.28 6.72-15 15-15h22.62c8.28 0 15 6.72 15 15v82.76c0 8.28-6.72 15-15 15zM281.37 262.76l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.86-5.86-15.36 0-21.21l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.35 0 21.21zM185.76 478.48v22.62c0 8.28-6.72 15-15 15H88c-8.28 0-15-6.72-15-15v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15zM270.69 698.63l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.36 5.86-21.21 0l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.85-5.86 15.35-5.86 21.21 0zM486.41 794.24h22.62c8.28 0 15 6.72 15 15V892c0 8.28-6.72 15-15 15h-22.62c-8.28 0-15-6.72-15-15v-82.76c0-8.28 6.72-15 15-15zM706.56 709.31l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.36 0 21.21l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.85-5.86-15.35 0-21.21zM802.17 493.59v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15v22.62c0 8.28-6.72 15-15 15h-82.76c-8.28 0-15-6.72-15-15zM717.24 273.44l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.86-5.86 15.36-5.86 21.21 0l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.35 5.86-21.21 0z" fill="#F9C626" p-id="5240"></path></svg>
          <svg  onClick={()=>setmaskFlag(true)} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="8994" width="32" height="32"><path d="M941.056 808.96l-288.256-285.184c79.36-120.832 69.12-280.576-32.768-381.952C502.784 25.6 308.736 30.72 186.368 153.6c-122.368 122.88-126.464 316.928-9.216 433.664 103.936 103.424 268.8 110.592 390.144 24.576l280.576 276.48c13.312 13.312 30.208 22.016 48.64 24.576 39.424 5.632 56.32-13.312 62.976-34.304 7.68-24.576 0-51.712-18.432-69.632zM549.376 513.536c-88.064 88.576-227.328 93.696-310.272 11.776-82.944-82.432-78.848-221.696 9.728-310.272 88.064-89.088 227.328-94.208 310.272-11.776 82.432 82.432 78.336 221.696-9.728 310.272z" fill="#040000" p-id="8995"></path></svg>
          </>
          :
         <>
          <Dropdown overlay={menu}>
          <Space>
          <svg  viewBox="0 0 1070 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="10404" width="32" height="32"><path d="M232.582 358.133c12.288 36.33 32.59 67.851 60.905 95.633 24.042-26.18 42.207-58.235 53.96-95.633H232.583z" p-id="10405" fill="#ffffff"></path><path d="M981.615 143.36H532.836L507.192 6.055H90.468C44.522 6.055 7.123 43.453 7.123 89.4v708.43c0 45.946 37.399 83.344 83.345 83.344h379.86l-30.453 137.305h541.74c45.947 0 83.345-37.398 83.345-83.344v-708.43c0-45.947-37.398-83.345-83.345-83.345zM415.833 564.358c-49.152-18.165-89.756-41.139-122.346-67.852-34.192 30.453-76.933 52.892-126.62 66.783l-17.096-28.316c48.618-12.822 89.222-32.055 121.277-59.303-33.124-33.658-56.097-72.66-68.92-117.003h-46.48v-32.056h121.277c-7.48-13.89-17.096-27.247-28.316-40.07l32.056-11.753c11.22 14.425 21.37 31.522 30.453 51.29h115.935v32.055h-46.481c-14.96 45.946-36.33 84.413-64.646 115.4 31.522 25.11 71.057 45.947 117.538 63.043l-17.631 27.782zM1023.288 934.6c0 22.974-18.7 41.673-41.673 41.673H492.232l20.837-95.633h156.538l-89.222-497.397-0.534 2.671-3.74-19.767 1.069 0.534-32.59-181.649h437.56c22.973 0 41.672 18.7 41.672 41.673V934.6z" p-id="10406" fill="#ffffff"></path><path d="M684.566 541.384h114.866v-30.453H684.566v-60.905h122.346v-30.453H648.771V638.62h162.95v-30.453H684.565v-66.783zM924.45 475.67c-9.616 0-18.164 1.603-26.178 5.877-7.48 3.74-14.96 9.617-20.837 17.096v-18.699h-34.727V638.62h34.727v-95.633c1.069-12.822 5.343-22.439 12.823-29.384 6.41-5.877 13.89-9.083 22.439-9.083 24.041 0 35.795 12.823 35.795 39.001v94.565h34.727v-97.77c1.069-43.275-19.233-64.646-58.769-64.646z" p-id="10407" fill="#ffffff"></path></svg>
          </Space>
          </Dropdown>
          <svg className='icon' onClick={changeSkin} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7054" width="32" height="32"><path d="M466.2784 386.048c-41.9328-115.2-35.0208-236.288 10.0864-340.5312A462.4896 462.4896 0 0 0 397.6704 66.56C158.5152 153.6 35.2256 418.048 122.2656 657.2032s351.488 362.4448 590.592 275.4048c123.9552-45.1072 216.7296-137.8816 265.3184-250.0608-215.8592 37.7856-434.3296-83.3536-511.8976-296.4992z" fill="#FFB612" p-id="7055"></path></svg>
          <svg onClick={()=>setmaskFlag(true)}  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="9403" width="32" height="32"><path d="M941.056 808.96l-288.256-285.184c79.36-120.832 69.12-280.576-32.768-381.952C502.784 25.6 308.736 30.72 186.368 153.6c-122.368 122.88-126.464 316.928-9.216 433.664 103.936 103.424 268.8 110.592 390.144 24.576l280.576 276.48c13.312 13.312 30.208 22.016 48.64 24.576 39.424 5.632 56.32-13.312 62.976-34.304 7.68-24.576 0-51.712-18.432-69.632zM549.376 513.536c-88.064 88.576-227.328 93.696-310.272 11.776-82.944-82.432-78.848-221.696 9.728-310.272 88.064-89.088 227.328-94.208 310.272-11.776 82.432 82.432 78.336 221.696-9.728 310.272z" fill="#ffffff" p-id="9404"></path></svg>
          </>
        }
       
        </div>
        </div>

        </header>


{/* //todo main*/}
      <main>
        <div className="content">
          <Outlet></Outlet>
        </div>
      </main>


      {/* footer */}
      <footer>
        <div  className='main'>
        法律声明隐私协议浙ICP备2022000356号-3浙公网安备 33011002011859号
        </div>
      </footer>

    {
      maskFlag && <Mask changeMask={setmaskFlag} ></Mask>
    }  
      
      <BackTop />
    </div>
  )
}

export default Index