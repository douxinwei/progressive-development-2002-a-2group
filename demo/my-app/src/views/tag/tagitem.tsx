import React,{ useEffect, useMemo, useState } from 'react'
import { useLocation } from 'react-router-dom'
import '../index/Quiz/QuizDetails/index.css'
import { useSelector, useDispatch } from "react-redux"
import { RouterDispatch, RouterStore, ClassList } from "../../utils/types"
import { Tag_List, RecommendList } from '../../api/api'
import Recommend from '../../components/Quiz/Recommend'
// import axios from '../../../../utils/request'
// import "../../Article/style/index.css"
//@ts-ignore
import { RouteItem } from '@/types/types'
import { NavLink, Outlet ,useNavigate} from 'react-router-dom'
import Classify from '../../components/Quiz/Classify'
import {
    EyeOutlined,
    ShareAltOutlined
} from '@ant-design/icons';
import { Empty } from 'antd';
// import axios from '../../utils/request'
// import {getArticleList}  from '../../utils/api'
interface Props {
    
}

const tagitem = (props: Props) => {
    const [i, setIndex] = useState(0)
    const localtion = useLocation()

    const navigate = useNavigate()

    let QuizDetailsList = localtion.state

    const dispatch:RouterDispatch = useDispatch()
    const newData = useSelector((state:RouterStore) => state.reducer.TagList)
    const comList = useSelector((state:RouterStore) => state.reducer.RecommendList)
    
    let data = useMemo(() => {
        let list:any = []
        return comList.filter((item:any,index:number)=>{
            if(item.tags[index]){
                list.push(item)
                return list
            }
        })
    }, [comList])

    const times = (stime:any) => {
        const tim = Math.round(
            (new Date().getTime() - new Date(stime.publishAt).getTime()) / 1000 /  60 /  60 / 24 
        )
        return tim
    }
    
    
    useEffect(()=>{
        dispatch(Tag_List())
        dispatch(RecommendList())
        // axios.get('/api/articlelist').then((res:any)=>{
        //     console.log(res,'123123312');
        // })
        
    },[dispatch])

    return (
        <div className='Quizdetails_container'>
            <div className="Quizdetails_container_left">
                <div className="Quizdetails_container_left_top">
                    <p>
                        <span className='p1'>{QuizDetailsList.label}</span>分类文章
                    </p>
                    <p>
                        共搜索到<span className='p1'>{QuizDetailsList.articleCount}</span>篇
                    </p>
                </div>
                <div className="Quizdetails_container_left_bottom">
                    <div className="bottom">
                        <div className="bottom_route">
                            <div className='bottom_b'>
                                <b>文章标签</b>
                            </div>
                            <div className="bottom_span">
                                {
                                    newData?.map((item:any, index:number) => {
                                        return <a onClick={()=>{
                                            setIndex(index)
                                            navigate(`/index/tag/${item.value}`, { state: item })
                                        }} className={item.label === QuizDetailsList.label ? "on" : ""} key={index}>{item.label}</a>
                                    })
                                }
                            </div>
                        </div>
                            
                        <div className="bottom_pro">
                            {
                                data && data.map((item:any,index:number)=>{
                                    return item.tags[0].value == QuizDetailsList.value ?  <div key={index} className="auiz_left_main"  
                                        onClick={()=>
                                            navigate('/index/Studydetails/' + item.id, {
                                            state: item
                                        })}
                                        >
                                        <div className='auiz_left_total'>
                                              <div className='auiz_header'>
                                                  <div className='auiz_left_container'>
                                                    <span>{item.title}</span>
                                                    <span className="auiz_left_container_span">|</span>
                                                    <span>{times(item)}天前</span>  
                                                  </div>
                                              </div>
                                              <main className='auiz_main'> 
                                                <div className='auiz_left_image'>
                                                  <img src={item.cover} alt="" />
                                                </div>
                                                <div className='auiz_left_icon'>
                                                  <EyeOutlined /><span>{item.views}</span>
                                                  <span>·</span>
                                                  <ShareAltOutlined/><span>Share</span>
                                                </div>
                                              </main>
                                        </div>
                                    </div> : []
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
            <div className="auiz_right">
                <Recommend></Recommend>
                <Classify></Classify>
            </div> 
        </div>
    )
}

export default tagitem
