import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import * as api from "@/api/getData"
import { Affix, Button } from 'antd';
import { AppDispatch, StoreType } from '@/store/modules'
import { DataItem } from '@/types/types'
import './style/index.scss'

type Props = {}

function Archive({ }: Props) {
  const dispatch: AppDispatch = useDispatch()
  const navigate = useNavigate()
  const [top, setTop] = useState(70);
  useEffect(() => {
    dispatch(api.get_list())
  }, [])
  let data = [...useSelector((state: StoreType) => state.reducer.data)]
  data && data.sort((a: DataItem, b: DataItem) => {
    return new Date(b.publishAt).getTime() - new Date(a.publishAt).getTime()
  })

  const reverseStr = (str:string)=>{
    switch (str) {
      case '01':

      return "January "
      case '02':

      return "february "
      case '03':

      return "JanuMarchary "
      case '04':

      return "April "
      case '05':

      return "May "
      case '06':

      return "June "
      case '07':

      return "July "
      case '08':

      return "August "
      case '09':

      return "September "
      case '10':

      return "October "
      case '11':

      return "November "
      case '12':

      return "December "
        
      default:
        break;
    }
  }
  return (
    <div className='classify'>
      <div className="left">
        <header>
          <h2>归档</h2>
          <p>共计{data.length}篇文章</p>
        </header>
        <main>

          {
            data && data.length > 0 ? data.map((item: DataItem, index: number) => {
              return <div key={item.id} onClick={() => navigate('/details' + item.id)}>
                {
                  index == 0 && <>
                    <h1>{item.publishAt.split('-')[0]}</h1>
                    <h2>{

                      reverseStr(  item.publishAt.split('-')[1])
                    

                    }</h2>
                  </>
                }
                {
                  data[index - 1] && item.publishAt.split('-')[0] != data[index - 1].publishAt.split('-')[0] &&
                  <h1>{item.publishAt.split('-')[0]}</h1>
                }
                {
                  data[index - 1] && item.publishAt.split('-')[1] != data[index - 1].publishAt.split('-')[1] &&
                  <h2 >
                    {

                    reverseStr(  item.publishAt.split('-')[1])

                    }
                  </h2>
                }
                <li >{item.title} <span>{moment(item.publishAt).format('MM-DD')}</span></li>
              </div>
            })
              : ""
          }
        </main>

      </div>


      <Affix className='right' offsetTop={top}>
        <div className="list">
          <h3>推荐阅读</h3>
          {
            data && data.filter((val: DataItem) => Number(val.views) > 3000).map((item: DataItem, index: number) => {
              return <li key={index} onClick={() => navigate('/index/details' + item.id)}>{item.title}</li>
            })
          }
        </div>
      </Affix>

    </div>
  )
}

export default Archive