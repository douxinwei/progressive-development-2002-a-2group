import  { useEffect, useState } from 'react'
import {  BackTop  } from "antd"
import "./style/index.scss"
import { thirdRouter } from '@/router/routerConfig'
import { DataItem, RouteItem } from '@/types/types'
import { NavLink, Outlet, useNavigate } from 'react-router-dom'
import { AppDispatch, StoreType } from '@/store/modules'
import { useDispatch, useSelector } from 'react-redux'
import * as api from "@/api/getData"
import { Affix, Tag } from 'antd'
import moment from 'moment'
import Banner from '@/components/Banner/Banner'
type Props = {}

function Article({}: Props) {
  const [ind,setInd] = useState(0)
  const dispatch:AppDispatch = useDispatch() 
  // 获取文章数据
  useEffect(()=>{
    dispatch(api.get_list())
  },[])

  const data = useSelector((state:StoreType)=>state.reducer.data)

  // nav 导航
  const [list,setlist] = useState(["所有","前端","后端","阅读","Linux","LeetCode","要闻"])

  // 锚点距离
  const [oftop, setTop] = useState(10);

const navigate = useNavigate()
  return (
    <div className='Article'>
      <div className="left">
        {/* 左侧头部组件 */}
        <div className="top">
         {
          //    ${data.filter((item:DataItem)=>item.lable==list[ind])}
          ind == 0?<Banner arr={data.slice(0,5)}></Banner>:`查询<${list[ind]}>分类文章xx篇`
       
          
         } 
        </div>
        {/* 左侧底部路由 */}
        <div className="bottom">
          <nav>
            {
              thirdRouter && thirdRouter.map((item:any,index:number)=>{
                return <NavLink key={index} to={item.path} 
                  onClick={()=>{
                    setInd(index)
                    // navigate('/index/article/'+ item.title ,{
                    //   state:item
                    // })
                  }}
                   className={index==ind ? 'on' :'router'} >{item.title}</NavLink>
              })
            }
          </nav>
          <div className="main">
            <Outlet></Outlet>
          </div>
        </div>
      </div>
      <Affix className="right" offsetTop={oftop}>
     <div>
     <div className="list">
      <h4>推荐阅读</h4>
      {
          data && data.filter((val:DataItem)=>Number(val.likes)>80).map((item:DataItem,index:number)=>{
            return <p key={index} onClick={() => navigate('/index/details' + item.id)}>{item.title} · <span style = {{color:"#ccc"}}>{moment(item.publishAt).fromNow()}</span></p>
          })
         }
      </div>
      <div className="list">
      <h4>文章标签</h4>
          {
          list && list.filter((val:string)=>val!=='所有').map((item:string,index:number)=>{
            return <Tag key={index}>{item}</Tag>
          })
         }
      </div>
     </div>
     </Affix>
     <BackTop />
    </div>
  )
}

export default Article