import React, { DOMElement, Fragment, useEffect, useState } from 'react'
import { useParams, useLocation, useNavigate } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import { DataItem } from '@/types/types'
import { SendOutlined } from '@ant-design/icons';
import moment from "moment"
import MyEditor from '@/components/MyEdictor/MyEditor';
import { getComment, get_details } from "@/api/getDetails"
import Login from "@/components/Login/Index"
import { Affix, Anchor,BackTop  } from "antd"
import CommentList from "@/components/Component/CommentList"
import Touch_bar from '@/components/Component/Touch_bar';
import { AppDispatch, StoreType } from '@/store/modules';
import * as api from "@/api/getData"
import './style/index.scss'
type Props = {}
type tocType =  {
    level?: string, id?: string, text?: string,children?:Array<tocType>
  }
interface ItemFC {
  id: string;
  title: string;
  cover: string;
  content: string;
  html: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  publishAt: string;
  toc:string;
  [key: string]: any;
}
function Details({ }: Props) {
  const { id } = useParams()
  const navigate = useNavigate()
  const data = useSelector((state: StoreType) => state.reducer.data)
  const [isLogin, setLogin] = useState<boolean>(false)
  const [oftop, setTop] = useState(10)
  const [likenum,setlikenum] = useState(0)
  const dispatch:AppDispatch = useDispatch()
  const { Link } = Anchor;
  // 评论状态
  const [num, setnum] = useState(1)
  const [toc, settoc] = useState([])
  const [textContent, setTextContent] = useState<ItemFC>({
    id: '',
    title: '',
    cover: '',
    content: '',
    html: '',
    views: 0,
    likes: 0,
    isRecommended: false,
    publishAt: '',
    toc: ''
  })

  

  useEffect(() => {
    // 文章动态
    get_details(id as string).then(res => {
        // console.log(res.data)
      setTextContent(res.data)
      settoc(JSON.parse(res.data.toc))
      setlikenum(res.data.likes)
    })
    dispatch(api.get_list())
  }, [])
  const readArry = useSelector((state:StoreType)=>state.reducer.data)
  // console.log(toc,'tttt')
  // 获取评论

  // 判断是否登录
  const mylogin = () => {
    let flag = localStorage.getItem('userinfo') ? false : true
    setLogin(flag)
  }
// 锚点
  const [list, setlist] = useState<Array<tocType>>([])
  useEffect(() => {
    let arr:Array<tocType> = []
    if (toc.length > 0) {
      toc.forEach((item:tocType) => item.children = [])
      toc.forEach((item :tocType)=> {
        if (item.level == '2') {
          arr.push(item)
        } else {
       arr[arr.length - 1].children?.push(item)
        }
      })
    }
    setlist(arr)
  },[toc])

  let ele = document.querySelector('.edit')
  let lis = [];
  if (ele) {
    let lis1 = ele.querySelectorAll('h2')
    let lis2 = ele.querySelectorAll('h3')
    let lis3 = ele.querySelectorAll('h4')
    lis = [...lis1,...lis2,...lis3]
    lis.forEach((item, index) => {
      toc.forEach((val:tocType) => {
        if (item.innerText == val.text) {
          item.setAttribute('id', val.text)
        }
      })
    })
  }



  return (
    <div className='details'>
      <div className="left">


        <div className="banner">
          <div className='swiper'><img src={textContent.cover} alt="" /></div>
        </div>
        <div className="text">

          <div className="content">
            <h1>{textContent.title}</h1>
            <dl><dd><p>{moment(textContent.publishAt).local().fromNow()}</p></dd></dl>


          </div>


          <div className='edit' >
            <MyEditor  flag={true} setTextContent={setTextContent} id={id as string} setNum={()=>setnum(num+1)}></MyEditor>
          </div>

        </div>



        {/* <div dangerouslySetInnerHTML={{__html:shtml}}></div> */}
        <h2 style={{ display: "flex", justifyContent: "center", margin: "20px 0" }} className='commenthead'>评论</h2>
        <div className="mymoment" style={{ marginBottom: "100px" }} onClick={mylogin}>
          <MyEditor setTextContent={()=>{}} setNum={() => setnum(num + 1)} flag={false} id={id as string}></MyEditor>
        </div>

        {/* 评论 */}



        <CommentList num={num} id={id as string}></CommentList>


{/* 左侧收藏 图标bar */}
        <div className="touch_bar">
       <Touch_bar id={id as string} num = {likenum} ></Touch_bar>
      </div>
      </div>
      <Affix offsetTop={oftop} className="right">
        <div>
          <div className="list">
            <h3>推荐阅读</h3>
            {
              readArry && readArry.filter((val: DataItem) => Number(val.views) > 3000).map((item: DataItem, index: number) => {
                return <p key={index} onClick={() => navigate('/index/details' + item.id)}>{item.title}</p>
              })
            }
          </div>
          <div className="list">
            <h4>目录</h4>

            <Anchor>
              {
                list && list.map((item ,index)=> {
                  return <Link href={`#${item.text}`} title={item.text} key={index}>
                    {item.children && item.children.map((val,ind)=>{
                      return <Link href={`#${val.text}`} title={val.text} key={ind}></Link>
                    })}
                  </Link>
                })
              }

            </Anchor>

          </div>
        </div>
      </Affix>


      {
        isLogin && <Login setLogin={setLogin} id={id as string}></Login>
      }

<BackTop />


     
    </div>
  )
}

export default Details