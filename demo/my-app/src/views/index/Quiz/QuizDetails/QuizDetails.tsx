import React,{ useEffect, useMemo, useState } from 'react'
import { useLocation } from 'react-router-dom'
import './index.css'
import { useSelector, useDispatch } from "react-redux"
import { RouterDispatch, RouterStore, ClassList } from "../../../../utils/types"
import { Tag_List, Classify_List, RecommendList } from '../../../../api/api'
// import axios from '../../../../utils/request'
import "../../Article/style/index.css"
import ComTag from '../../../../components/Quiz/ComTag'
//@ts-ignore
import { RouteItem } from '@/types/types'
import { NavLink, Outlet ,useNavigate} from 'react-router-dom'
import {
    EyeOutlined,
    ShareAltOutlined
} from '@ant-design/icons';
interface Props {
    
}

const QuizDetails = (props: Props) => {
    const dispatch:RouterDispatch = useDispatch()

    const localtion = useLocation()

    let QuizDetailsList = localtion.state
    
    // const getLists = async () => {
    //     let res = await api.getSelection();
    //     if (res.data?.statusCode === 200) {
    //         setList(res.data?.data || [])
    //     }
    // }
    // const getList = async () => {
    //     let res = await api.getKnowledge();
    //     if (res.data?.statusCode === 200) {
    //         setArr(res.data?.data || [])
    //     }
    // }
    // useEffect(()=>{
    //     getList()
    // },[])

    // 筛选不可行方法之一
    // const newQuizList = useMemo(()=>{
    //     let arr:Array<any> = []
    //     RecommendList2.forEach((item:any) =>{
    //         console.log(item.id,'2sss');
    //         if(item.id === QuizDetailsList.id) {
    //             arr.push(item)
    //         }
    //     })
    //     return arr
    // },[QuizDetailsList,RecommendList2])

    const navigate = useNavigate()
    const StudyList = useSelector((state:RouterStore) => state.reducer.ClassifyList)
    const comList = useSelector((state:RouterStore) => state.reducer.RecommendList)

    let data = useMemo(() => {
        let list:any = []
        return comList.filter((item:any,index:number)=>{
            if(item.tags[index]){
                list.push(item)
                return list
            }
        })
    }, [comList])

    data.forEach((item:any)=>{
        console.log(item.category.label);
        
    })
    const [ind,setInd] = useState(0)

    const times = (stime:any) => {
        const tim = Math.round(
            (new Date().getTime() - new Date(stime.publishAt).getTime()) / 
            1000 / 
            60 / 
            60 / 
            24 
        )
        return tim
    }

    useEffect(()=>{
        dispatch(Tag_List())
        dispatch(Classify_List())
        dispatch(RecommendList())
        // axios.get('/https://bjwz.bwie.com/wipi/api/tag?articleStatus=publish').then(res=>{
        //     console.log(res);
        // })
    },[dispatch])
    return (
        <div className='Quizdetails_container'>
            <div className="Quizdetails_container_left">
                <div className="Quizdetails_container_left_top">
                    <p>
                        <span className='p1'>{QuizDetailsList.label}</span>分类文章
                    </p>
                    <p>
                        共搜索到<span className='p1'>{QuizDetailsList.articleCount}</span>篇
                    </p>
                </div>
                <div className="Quizdetails_container_left_bottom">
                    <div className="bottom">
                        <nav>
                            {
                                StudyList && StudyList.map((item:any,index:number)=>{
                                    return <span key={index} onClick={()=>{
                                    setInd(index)
                                    navigate('/index/quiz/'+ item.value,{
                                        state:item
                                    })}} className={item.label==QuizDetailsList.label ? 'on' :'router'} >{item.label}</span>
                                })
                            }
                        </nav>
                        <div className="bottom_pro">
                        {
                                data && data.map((item:any,index:number)=>{
                                    return item.category.label == QuizDetailsList.label ?  <div key={index} className="auiz_left_main"  
                                        onClick={()=>
                                            navigate('/index/Studydetails/' + item.id, {
                                            state: item
                                        })}
                                        >
                                        <div className='auiz_left_total'>
                                              <div className='auiz_header'>
                                                  <div className='auiz_left_container'>
                                                    <span>{item.title}</span>
                                                    <span className="auiz_left_container_span">|</span>
                                                    <span>{times(item)}天前</span>  
                                                  </div>
                                              </div>
                                              <main className='auiz_main'> 
                                                <div className='auiz_left_image'>
                                                  <img src={item.cover} alt="" />
                                                </div>
                                                <div className='auiz_left_icon'>
                                                  <EyeOutlined /><span>{item.views}</span>
                                                  <span>·</span>
                                                  <ShareAltOutlined/><span>Share</span>
                                                </div>
                                              </main>
                                        </div>
                                    </div> : []
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
            <ComTag></ComTag>
        </div>
    )
}

export default QuizDetails
