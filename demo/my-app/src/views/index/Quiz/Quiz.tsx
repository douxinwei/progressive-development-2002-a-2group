import React from 'react'
import "./Quiz.css"
import QuizPublic from '../../../components/Quiz/QuizPublic'
import Recommend from '../../../components/Quiz/Recommend'
import Classify from '../../../components/Quiz/Classify'
import '../../Index.css'

type Props={}
function Quiz({ }: Props) {
  
  return (
    <div className='auiz'>
      <QuizPublic></QuizPublic>
      <div className="auiz_right">
        <Recommend></Recommend>
        <Classify></Classify>
      </div>
    </div>
  )
}

export default Quiz