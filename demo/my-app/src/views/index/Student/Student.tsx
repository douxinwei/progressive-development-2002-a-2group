import { useState,useEffect } from 'react'
import { useSearchParams,useNavigate } from 'react-router-dom'
// import { AllState } from '../store/index'
import { useSelector, useDispatch } from 'react-redux'
import { getUserInfo } from '@/api/getDetails'
import { AppDispatch, StoreType } from '@/store/modules'
function Student() {
    const [count, setCount] = useState(0)
    const [params,setParams] = useSearchParams()
    let articleid = useSelector((state:StoreType)=>{
        return state.reducer.articleId
    })
    const navigate = useNavigate()
    const dispatch:AppDispatch = useDispatch()
    useEffect(()=>{
        console.log(params.get('code'),'回调页面参数')
        getUserInfo(params.get('code')).then((res)=>{
            dispatch({
                type:'changeUserInfo',
                payload:true
            })
            console.log('回跳页面的id',articleid)
            navigate("/article"+articleid)
            console.log('登录返回值',res)
            localStorage.setItem('userinfo',JSON.stringify(res.data))
        })
    },[])
  return (
    <div className="shorp">
      学生页
    </div>
  )
}

export default Student
