import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useLocation, useParams, useNavigate } from 'react-router-dom'
import request from '../../utils/request'
import { formatDate } from '../../utils/Time'
import { getArticleDetail } from '../../utils/api'
import { Input, Button, Badge, BackTop, Affix, Anchor, Modal, } from 'antd';
import { CommentOutlined, ShareAltOutlined, HeartFilled, VerticalAlignTopOutlined } from '@ant-design/icons';
import MarkNav from "markdown-navbar";
import './articleDetail.css'
import QRCode from 'qrcode.react'
const { TextArea } = Input;
interface ARTTYPE {
    id: string;
    likes: number;
    publishAt: string
    status: string
    title: string
    views: number
}
interface ItemFC {
    id: string;
    title: string;
    cover: string;
    content: string;
    html: string;
    views: number;
    likes: number;
    isRecommended: boolean;
    publishAt: string;
    [key: string]: any;
}
type Props = {}

const ArticleDetail = (props: Props) => {
    let params = useParams<{ id: string }>()
    const konwledge = useRef(null);
    const navigate = useNavigate()
    // 吸顶
    const [top, setTop] = useState(10);
    // 点赞
    const [flag, setflag] = useState(false)
    // 分享弹窗
    const [makeflag, setmakeflag] = useState(false)
    const canvasDom = useRef(null)
    const [downurl, setDownurl] = useState('')
    const [textDeleteContent, setTextDeleteContent] = useState<ItemFC>({
        id: '',
        title: '',
        cover: '',
        content: '',
        html: '',
        views: 0,
        likes: 0,
        isRecommended: false,
        publishAt: ''

    })
    // 获取id来的详情页数据
    useEffect(() => {
        // setcanvas()
        getArticleDetail((params.id as string)).then((res: any) => {
            console.log(res.data, '文章详情')
            setTextDeleteContent(res.data)
        }, (err: any) => {
            console.log(err, '请求失败')
        })
    }, [])
    // swiper
    const [swiperlist, setswiperList] = useState<ARTTYPE[]>([])
    useEffect(() => {
        request.get('/api/swiperlist').then((res: any) => {
            console.log(res.data, 'swiper')
            setswiperList(res.data)
        })
    }, [])
    const btn_like = () => {
        setflag(!flag)
        if (flag) {
            textDeleteContent.likes--;
        } else {
            textDeleteContent.likes++;
        }
    }
    // 分享弹框
    const setmake = () => {
        setmakeflag(!makeflag)
    }
    return (
        <div className='detail_home' style={{
            height: '600vh',
            padding: 8,
        }}>
            <div className={makeflag ? 'make' : 'make show'}>
                {/* 图片下载 */}
                <div className='makedownload'>
                    <div className='canimg'>
                        <img src={downurl} alt="" />
                    </div>
                    <p>{textDeleteContent.title}</p>
                    <div className='btn'>
                        <span>
                            <button onClick={() => {
                                setmake()
                            }} className="close">
                                <a href="">关闭</a>
                            </button>
                        </span>
                        <span>
                            <button><a href={downurl} download="canvas.png" className='submit'>下载</a></button>
                        </span>
                    </div>
                    <canvas id="canva" ref={canvasDom}></canvas>
                    <QRCode
                        style={{
                            marginLeft: "-250px",
                            marginTop: '300px'
                        }}
                        id="qrcode"
                        value={window.location.href} //value参数为生成二维码的链接 我这里是由后端返回
                        size={100} //二维码的宽高尺寸
                    />
                </div>
            </div>
            <div className='detail_left_home'>
                <img width="100%" height="500px" src={textDeleteContent.cover} alt="" />
                <h3 style={{ fontSize: '30px', fontWeight: 'bold', textAlign: 'center', marginTop: '20px' }}>{textDeleteContent.title}</h3>
                <p style={{ textAlign: 'center', fontStyle: 'oblique' }}>发布于{textDeleteContent.createAt}·阅读量{textDeleteContent.views}</p>
                <span style={{ color: '#000' }}>{textDeleteContent.content}</span>
                {/* html,content */}
                <div
                    style={{ color: '#000' }}
                    ref={konwledge}
                    dangerouslySetInnerHTML={{ __html: textDeleteContent.html }}
                    className="boxs markdown"
                ></div>

                {/* 评论 */}
                <p className='sorker' style={{ color: '#000' }}>评论</p>
                <TextArea style={{ width: '95%', height: '200px', marginLeft: '20px' }} rows={4} placeholder="hello" maxLength={6} />
                <Button type="link" className='detail_publish'>发布</Button>
                {/* 喜欢 */}
                <span className='heart_likes'>
                    <Badge count={textDeleteContent.likes} className='badge_likes' >
                        {
                            flag ?
                                <HeartFilled className='header_icon' style={{ position: 'fixed', left: '240px', top: '300px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%', color: 'red' }} onClick={() => btn_like()} />
                                :
                                <HeartFilled className='header_icon' style={{ position: 'fixed', left: '240px', top: '300px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%', }} onClick={() => btn_like()} />
                        }
                        {/* <HeartFilled className='header_icon' style={{ position: 'fixed', left: '240px', top: '300px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%',}} onClick={() => btn_like()}  /> */}
                    </Badge>
                </span>
                {/* 评论 */}
                <CommentOutlined className='likes_icon1' style={{ position: 'fixed', left: '240px', top: '350px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%' }} />
                {/* 分享文章图标 */}
                <ShareAltOutlined className='likes_icon2' style={{ position: 'fixed', left: '240px', top: '400px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%' }} onClick={setmake} />
            </div>
            {/* 吸顶 */}
            <Affix offsetTop={top}>
                <div className='detail_right_home'>
                    <div className='content_T_detail'>
                        <b style={{ marginLeft: '10px', lineHeight: '30px' }}>推荐阅读</b>
                        {
                            swiperlist && swiperlist.map((item: any, index: number) => {
                                return <a href={"/index/article/" + item.id} key={index}><div key={index}>
                                    <p style={{ marginTop: '2px' }} className="detail_right_reading"><span style={{ marginLeft: '10px', marginTop: '20px' }}>{item.title}</span>&emsp;<span>{formatDate(item.createAt)}</span>
                                    </p>
                                </div>
                                </a>
                            })
                        }
                    </div>
                    <div className='content_B_detail'>
                        <div>
                            <Anchor>
                                {textDeleteContent.content ?
                                    <div style={{ fontSize: "18px", fontWeight: "600" }}>
                                        目录
                                    </div>
                                    : " "
                                }
                                <MarkNav
                                    className="floorbox"
                                    source={textDeleteContent && textDeleteContent.content}
                                    headingTopOffset={-5}
                                ></MarkNav>
                            </Anchor>
                        </div>
                    </div>
                </div>
            </Affix>

            {/* 返回顶部 */}
            <BackTop>
                <div className='updown'><VerticalAlignTopOutlined /></div>
            </BackTop>

        </div >
    )
}

export default ArticleDetail
// style={{ position: 'fixed', left: '240px', top: '300px', width: '30px', height: '30px', lineHeight: '34px', fontSize: '18px', border: '1px solid #ccc', borderRadius: '50%' }}