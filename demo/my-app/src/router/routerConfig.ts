import { lazy } from "react"
import { RouteList } from '../types/types'

// 路由表
// 三级路由

export let thirdRouter = [
    {
        path: '/index/article/all',
        component: lazy(() => import('../views/Third-douter/All/All')),
        title:'所有'
    },
    {
        path: '/index/article/front',
        component: lazy(() => import('../views/Third-douter/Front/Front')),
        title:'后端2'
    },
    {
        path: '/index/article/back',
        component: lazy(() => import('../views/Third-douter/Back/Back')),
        title:'前端'
    },

    {
        path: '/index/article/read',
        component: lazy(() => import('../views/Third-douter/Read/Read')),
        title:'js'
    },
    {
        path: '/index/article/leeCode',
        component: lazy(() => import('../views/Third-douter/LeeCode/LeeCode')),
        title:'sass'
    },
    {
        path: '/index/article/linux',
        component: lazy(() => import('../views/Third-douter/Linux/Linux')),
        title:'json'
    },
    {
        path: '/index/article/news',
        component: lazy(() => import('../views/Third-douter/News/News')),
        title:'C++'
    },

]
// 二级路由
export const secRouter = [
    {
        path: '/index/article',
        component: lazy(() => import('../views/index/Article/Article')),
        children: thirdRouter
    },
    {
        path: '/index/archive',
        component: lazy(() => import('../views/index/Archive/Archive'))
    },
    {
        path: '/index/quiz',
        component: lazy(() => import('../views/index/Quiz/Quiz'))
    },
    {
        path: '/index/new',
        component: lazy(() => import('../views/index/New/New'))
    },
    {
        path: '/index/details:id',
        component: lazy(() => import('../views/index/Details/Details'))
    },
]


const routes: RouteList = [
    {
        path: '/',
        redirect: '/index/article/all',
    },
    {
        path: '/student',
        component: lazy(() => import('../views/index/Student/Student')),
    },
    {
        path: '/index',
        component: lazy(() => import('../views/Index')),
        children: secRouter
    },
    {
        path: '/detail',
        component: lazy(() => import('../views/Details/Details')),
    },


]


export default routes