import { FC, Suspense } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import routes from './routerConfig'
import { RouteList, RouteItem } from '../types/types'

type Props = {}

// Function Component
const Index: FC<Props> = (props: Props) => {
    const renderDom = (routes: RouteList) => {
        return routes.map((item: RouteItem, index: number) => {
            return <Route
                key={index}
                path={item.path}
                element={item.redirect ? <Navigate to={item.redirect}/> : <item.component />}
            >
                {
                    item.children && renderDom(item.children)
                }
            </Route>
        })
    }

  return (
    <Suspense fallback={<div>加载中...</div>}>
        <BrowserRouter>
            <Routes>
                {
                    renderDom(routes)
                }
            </Routes>
        </BrowserRouter>
    </Suspense>
  )
}

export default Index