import axios from './request'

/*
首页文章列表
*/
export function getArticleList(){
    return axios.get('/api/articlelist')
    // https://bjwz.bwie.com/wipi/api/knowledge
}

/*
首页轮播图数据
*/
export function getSwiperList(){
    return axios.get('/api/swiperlist')
}

/*获取文章详情 */
export function getArticleDetail(id:string){
    return axios.post('/api/article/'+id)
}
