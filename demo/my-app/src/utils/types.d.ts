import { JSXElement } from 'react'
import store from "../store"
// 路由列表
export type RouteList = Array<RouteItem>

// 路由项
export type RouteItem = {
    path: string,
    redirect?: string,
    component?: JSXElement,
    children?: Array<RouteItem>
}

export type RouterDispatch = typeof store.dispatch

export type RouterStore = {
    reducer:Array
    StudyList: Array<ListItem>
    RecommendList:Array<ItemList>
    ClassifyList:Array<ClassList>
    TagList:Array<ClassList>
}
<<<<<<< HEAD
=======

>>>>>>> ee239f202012abf758190bb4b82940dc16bd4821
//知识小测数据
export type ListItem = {
    id:string,
    content:string,
    cover:string
    createAt:string
    html:string
    isCommentable:boolean
    likes:number
    order:number
    parentId:string
    publishAt:string
    status:string
    summary:string
    title:string
    toc:string
    updateAt:string
    views:number
}
// 文章数据
export type ItemList = {
    id:string,
    title:string,
    cover:string,
    summary:string,
    content:string,
    html:string,
    toc:string,
    status:string,
    views:number,
    likes:string,
    isRecommended:boolean,
    needPassword:boolean,
    totalAmount:string,
    isPay:boolean,
    isCommentable:boolean,
    publishAt:string,
    createAt:string,
    updateAt:string,
    tags:Array<string>,
    category:Array<string>
}

// 标签数据
export type ClassList = {
    articleCount:number,
    createAt:string,
    id:string,
    label:string,
    updateAt:string,
    value:string,
}