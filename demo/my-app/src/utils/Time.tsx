export const formatDate = (dateStr: string) => {
    let distanceTime = new Date().getTime() - new Date(dateStr).getTime()
    let day = 1000 * 60 * 60 * 24
    let hour = 1000 * 60 * 60
    let mine = 1000 * 60
    if (distanceTime / day > 1) {
        return Math.floor(distanceTime / day) + '天前'
    } else if (distanceTime / hour > 1) {
        return Math.floor(distanceTime / hour) + '小时前'
    } else if (distanceTime / mine > 1) {
        return Math.floor(distanceTime / mine) + '分钟前'
    } else {
        return '刚刚'
    }
}