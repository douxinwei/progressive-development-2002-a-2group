import { Button, Modal,  Form, Input } from 'antd';
import React, { Fragment, useEffect, useState } from 'react';
import {
  WechatOutlined ,
  QqOutlined ,
  WeiboCircleOutlined 
} from '@ant-design/icons';
import { AppDispatch } from '@/store/modules';
import { useDispatch } from 'react-redux';
type Props = {
  setLogin:Function,
  id:string
}

function Index({setLogin,id}: Props) {
  const [isModalOpen, setIsModalOpen] = useState(true);
  
  const [form] = Form.useForm()
  const dispatch:AppDispatch = useDispatch()
  const showModal = () => {
    setLogin(true)
  };

  const handleOk = () => {
    setLogin(false)
  };

  const handleCancel = () => {
    setLogin(false)

  };

  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <Fragment>
<Modal title="登录" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer={null}>
<Form
      name="basic"
      form={form}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>

   <footer style={{display:"flex",justifyContent:"center",margin:"20px 0"}}>
    <li><WechatOutlined style={{fontSize:"30px",margin:"0 10px"}}/></li>
    <li><QqOutlined 
    onClick={()=>{
      dispatch({
        type:"CHANGEID",
        payload:id
      });
      window.open('http://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101977975&redirect_uri=http://www.yiluxdeng.com/student&state=test')
    }}
    style={{fontSize:"30px",margin:"0 10px"}}/></li>
    <li><WeiboCircleOutlined style={{fontSize:"30px",margin:"0 10px"}}/></li>
   </footer>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
    <footer>
   
    </footer>
      </Modal>
    </Fragment>
  )
}

export default Index