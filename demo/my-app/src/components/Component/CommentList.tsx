import { getComment } from '@/api/getDetails'
import React, { createElement, useEffect, useState } from 'react'
import { DislikeFilled, DislikeOutlined, LikeFilled, LikeOutlined } from '@ant-design/icons';
import { Avatar, Comment, Tooltip,List } from 'antd';

type Props = {
    id:string
    num:number
}

function CommentList({id,num}: Props) {
    const [comment,setComment] = useState([{}])
 // 评论动态
useEffect(()=>{
    getComment(id).then(res=>{
        setComment(res.data)
     })

},[id,num])



const [likes, setLikes] = useState(0);
const [dislikes, setDislikes] = useState(0);
const [action, setAction] = useState<string | null>(null);

const like = () => {
  setLikes(1);
  setDislikes(0);
  setAction('liked');
};
const dislike = () => {
  setLikes(0);
  setDislikes(1);
  setAction('disliked');
};

const actions = [
  <Tooltip key="comment-basic-like" title="Like">
    <span onClick={like}>
      {createElement(action === 'liked' ? LikeFilled : LikeOutlined)}
      <span className="comment-action">{likes}</span>
    </span>
  </Tooltip>,
  <Tooltip key="comment-basic-dislike" title="Dislike">
    <span onClick={dislike}>
      {React.createElement(action === 'disliked' ? DislikeFilled : DislikeOutlined)}
      <span className="comment-action">{dislikes}</span>
    </span>
  </Tooltip>,
  <span key="comment-basic-reply-to">Reply to</span>,
];


  return (
    <div className='comment' >
    
     {/* {
        comment && comment.map((item:any,index:number)=>{
          return <li key={index} dangerouslySetInnerHTML={{__html:item.html}}></li>
        })
      } */}

{
        comment &&   <List
        className="comment-list"
        itemLayout="vertical"
        dataSource={comment}
        pagination={
            {
                      pageSize: 3,
            }
        }
        renderItem={(item:any) => (
        <Comment
          actions={actions}
          author={<a>{item.name}</a>}
          avatar={
            <span
                                style={{
                                    width: '28px',
                                    height: '28px',
                                    fontSize: '18px',
                                    color: '#fff',
                                    lineHeight: '28px',
                                    textAlign: 'center',
                                    background: `#${Math.random().toString(16).slice(-6)}`,
                                    borderRadius: '50%',
                                    display: 'block',
                                }}
                            >
                                {item.name && item.name.slice(0, 1)}
                            </span>
          }
          content={
            <p>
             {item.content}
            </p>
          }
          datetime={
            <Tooltip title="2016-11-22 11:22:33">
              <span>8 hours ago</span>
            </Tooltip>
          }
        />
        )}/>
      }
    </div>
  )
}

export default CommentList