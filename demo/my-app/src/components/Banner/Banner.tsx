import React from 'react'
import { Carousel } from 'antd';

import { useNavigate } from 'react-router-dom';
import { DataItem } from '@/types/types';
type Props = {
  arr:Array<DataItem>
}

const contentStyle: React.CSSProperties = {
    height: '100%',
    color: '#fff',
    lineHeight: '100%',
    textAlign: 'center',
    background: '#364d79',
  };
  
function Banner({arr}: Props) {
  const formmatDate:any = (str:string)=>{
    let diffTime:number = new Date().getTime() - new Date(str).getTime()
    let day = 1000*60*60*24
    let hour = 1000*60*60
    if(diffTime/day <1){
      return Math.floor(diffTime/hour) +'小时'
    }else{
      return Math.floor(diffTime/day) + '天'
    }

  }
 
  const navigate = useNavigate()
  return (
    <Carousel autoplay>
   {
    arr.map((item:DataItem,index:number)=>{
        return <div key={index} className='swiper' onClick={()=>navigate('/details'+item.id)}><img src={item.cover} alt="" />
        <div className="content">
          <h3>{item.title}</h3>
          <p><span>{formmatDate(item.publishAt)}前</span><b> · </b>  <span>{item.views}次阅读</span></p>
        </div>
        </div>
    })
   }
  </Carousel>
  )
}

export default Banner