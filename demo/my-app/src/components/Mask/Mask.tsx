import React, { useEffect, useState } from 'react'
import { Input, Space } from 'antd';
import './mask.scss'
import { AppDispatch, StoreType } from '@/store/modules';
import { useDispatch, useSelector } from 'react-redux';
import * as api from "@/api/getData"
import { DataItem } from '@/types/types';
const { Search } = Input;
type Props = {

  changeMask: Function
}

function Mask({ changeMask }: Props) {
  const [inputStatus, setStatus] = useState<string>('')
  const [searchList, setsearchList] = useState([])
  const dispatch:AppDispatch=useDispatch()
  useEffect(()=>{
    dispatch(api.get_list())
  },[])
  const SuggestedReadingList = useSelector((state:StoreType)=>state.reducer.data)
  const onSearch = (value: string) => {
    if (value != '') {
      const arr= SuggestedReadingList.filter((item)=> {//模糊搜索
        return item.title.includes(value)
      })
      setsearchList(arr)
    } else {
      setsearchList([])
    }
  }
  window.addEventListener('keydown', (e) => {
    if (e.key == 'Escape') {
      changeMask(false)
    }
  })
  useEffect(() => {
    return () => {
      window.removeEventListener('keydown', (e) => { })
    }
  }, [])
  // 输入的时候改变输入框状态
  const input = (e: any) => {
    if (e.target.value) {
      setStatus('')
    } else {
      setStatus('error')
    }
  }

  return (
    <>
      {
        <div className="mask">
          <div className="maskTop"><div className="left">搜索</div><div className="right" onClick={() => changeMask(false)}>ESC</div></div>
          <Search status={inputStatus as any} onMouseOver={() => setStatus('error')} onFocus={() => setStatus('error')} onInput={(e) => input(e)} onBlur={() => setStatus('')} onMouseLeave={(e) => input(e)} placeholder="input search text" onSearch={onSearch} enterButton />
          <main>
            {
              searchList.length > 0 && searchList.map((item: any) => {
                return <li key={item.id}>{item.title}</li>
              })
            }
          </main>
        </div>
      }

    </>

  )
}

export default Mask