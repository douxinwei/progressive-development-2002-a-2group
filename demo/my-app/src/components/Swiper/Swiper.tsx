import React from 'react'
import { Carousel } from 'antd';

type Props = {
  sweiperList: any
}

const Swiper = (props: Props) => {
  const { sweiperList } = props
  const contentStyle: React.CSSProperties = {
    height: '260px',
    color: '#fff',
    lineHeight: '260px',
    textAlign: 'center',
    background: '#364d79',
  };

  return (
    <div>
      <Carousel autoplay>
        {
          sweiperList.map((item: any, index: number) => {
            return <div key={index}>
              <h3 style={contentStyle}>{item.title}</h3>
            </div>
          })
        }

      </Carousel>

    </div>

  )
}

export default Swiper