import { LikeOutlined, MessageOutlined,ShareAltOutlined} from '@ant-design/icons';
import React, { useState } from 'react';
import moment from "@/utils/moment"
import {useNavigate} from "react-router-dom"
import './style/index.scss'
import { Button, Image, Space } from 'antd';
import { DataItem } from '@/types/types';
type Props = {
    data:Array<DataItem>
}

function List({data}:Props) {
  const navigate = useNavigate()
  
  return (
    <>
   {

data.map((item:DataItem,index:number)=>{
    return <div className='ele' key={index} 
    onClick={()=>navigate('/index/details'+item.id,{state:item})}
    > <h4>{item.title}</h4>
      <dl >
        <dt><Image src={item.cover}></Image> </dt>
        <dd>
          <p>{item.summary}</p>
          <div className='bar'>
            <li><LikeOutlined /> {item.likes}点赞</li>
            <li><MessageOutlined />评论</li>
            {/* <li>{moment(item.publishAt).local().fromNow()}</li> */}
          </div>
        </dd>
        
        </dl>
    
    </div>
  })

   }
    </>
  )
}

export default List