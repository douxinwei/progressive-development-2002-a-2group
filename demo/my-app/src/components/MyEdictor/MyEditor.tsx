import '@wangeditor/editor/dist/css/style.css' // 引入 css

import React, { useState, useEffect } from 'react'
import { Editor, Toolbar } from '@wangeditor/editor-for-react'
import { IDomEditor, IEditorConfig, IToolbarConfig } from '@wangeditor/editor'
import { getComment, get_details, postComment } from '@/api/getDetails'
interface props  {
    flag:boolean
    id:string
    setNum:Function
    setTextContent:Function
    // setISlogin:React.FunctionComponent
}
function MyEditor({flag,id,setNum,setTextContent}:props) {
    // editor 实例
    const [editor, setEditor] = useState<IDomEditor | null>(null)   // TS 语法
    // const [editor, setEditor] = useState(null)                   // JS 语法
    // 编辑器内容
    const [html, setHtml] = useState('')

    // 模拟 ajax 请求，异步设置 html
    useEffect(() => {
        get_details(id).then(res=>{
          flag ? setHtml(res.data.html) :setHtml('')
           setTextContent(res.data)
         })
    }, [id])

    // 工具栏配置
    const toolbarConfig: Partial<IToolbarConfig> = {

        toolbarKeys:[
            "emotion"
        ]
     }  // TS 语法
    // const toolbarConfig = { }                        // JS 语法

    // 编辑器配置
    const editorConfig: Partial<IEditorConfig> = {    // TS 语法
    // const editorConfig = {                         // JS 语法
        placeholder: '请输入内容...',
        readOnly:flag,
        MENU_CONF:{
            emotion:{
                emotions: '😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 🙃 😉'.split(' ') // 数组
            }
        }
    }
   

    // 及时销毁 editor ，重要！
    useEffect(() => {
        return () => {
            if (editor == null) return
            editor.destroy()
            setEditor(null)
        }
    }, [editor])

let user = JSON.parse(localStorage.getItem('userinfo') as string)
    const publish = ()=>{
        console.log(editor?.getText())
        setNum()
        postComment({
// articleId,name,email,content,html,url,pass,userAgent,hostId,parentCommentId,replyUserName ,replyUserEmail
        articleId:id,
        name:user.name,
        email:user.email,
        content:editor?.getText(),
        html:editor?.getHtml(),
        pass: '1',
        url:'1',
        userAgent : '1' ,
        hostId: '1',
        parentCommentId: '1',
        replyUserName: '1' ,
        replyUserEmail: '1',
        }).then(res=>{
            console.log(res.data)
            editor?.setHtml('')
           
        })
        
    }


   

    return (
        <>
            <div style={{ zIndex: 100}} >
              
              <Editor
                defaultConfig={editorConfig}
                value={html}
                onCreated={setEditor}
                mode="default"
                style={{minHeight:"100px",  overflowY: 'hidden'}}
                
            />
            {
                !flag &&  <Toolbar
                editor={editor}
                defaultConfig={toolbarConfig}
                mode="default"
                style={{ borderBottom: '1px solid #ccc' }}
            />
            }
              
            </div>
            {
                !flag && <button onClick={publish}>发布</button>
            }
           
        </>
    )
}

export default MyEditor