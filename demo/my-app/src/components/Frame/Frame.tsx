import React, { useState, useEffect } from 'react'
import "./Frame.css"

type Props = {
   SuggestedReadingList: any,
   titleOne: any,
   titleTwo: any,
   classifyList:any
}


const Frame = (props: Props) => {
   console.log(props)
   const { SuggestedReadingList, titleOne, titleTwo,classifyList } = props
   const [title1, settitle1] = useState("标题1")
   const [title2, settitle2] = useState("标题2")
   const [list1, setlist1] = useState([{id:1,title:"111"}])
   const [list2, setlist2] = useState([{id:1,title:"111"}])
   useEffect(() => {
      settitle1(titleOne)
      settitle2(titleTwo)
      setlist1(SuggestedReadingList)
      setlist2(classifyList)
   }, [])
   useEffect(() => {
      setlist1(SuggestedReadingList)
   }, [SuggestedReadingList])
   return (
      <div>
         <div className='FrameTop'>
            <div className="title"><b>{title1}</b></div>
            {
              list1.length>0 && list1.map((item: any, index) => {
                  return <div key={index}><b>{item.title}</b></div>
               })
            }
         </div>
         <div className='FrameDown'>
            <div className="title"><b>{title2}</b></div>
            {
               list2.map((item:any, index) => {
                  return <div key={index}>
                     <p><b>{item.title}{item.num}篇</b></p>
                     </div>
               })
            }
         </div>
      </div>
   )

}

export default Frame