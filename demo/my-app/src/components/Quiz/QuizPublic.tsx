import React,{ useEffect, useState } from 'react'
import { useSelector, useDispatch } from "react-redux"
import { getStudyList } from "../../api/api"
import { RouterDispatch, RouterStore, ListItem } from "../../utils/types"
import { getArticleList, getSwiperList } from '../../utils/api'
import Article,{ArticleResponseTypeItemFC} from './Article'
import {
  EyeOutlined,
  ShareAltOutlined
} from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

interface ItemFC{
	id: string;
	title: string;
	cover: string;
	content: string;
	html: string;
	views: number;
	likes: number;
	isRecommended: boolean;
	publishAt: string;
	[key:string]:any;
}

const QuizPublic = (props: any) => {

    const dispatch:RouterDispatch = useDispatch()
    const navigate = useNavigate()

    const [textContent,setTextContent] = useState<ItemFC>({
      id: '',
      title: '',
      cover: '',
      content:  '',
      html:  '',
      views: 0,
      likes: 0,
      isRecommended: false,
      publishAt: ''
    })

    const StudyList = useSelector((state:RouterStore) => state.reducer.StudyList)

    const times = (stime:any) => {
        const tim = Math.round(
            (new Date().getTime() - new Date(stime.publishAt).getTime()) / 
            1000 / 
            60 / 
            60 / 
            24 
        )
        return tim
    }
    
    useEffect(()=>{
        dispatch(getStudyList())
        getArticleList().then((res:any)=>{
          console.log(res);
        })
        getSwiperList().then((res:any)=>{
          setTextContent(res.data)
        })
    },[dispatch])
    
    console.log(textContent);
    
  
    return (
        <div className="auiz_left">
          {/* {
            StudyList?.map((articleItem:any,index:number)=>{
              return  <Article itemValue={articleItem} key={index}></Article>
            })
          } */}
        {
          StudyList?.map((item:any,index:number)=>{
            return <div key={index} className="auiz_left_main"  onClick={()=>
              navigate('/index/Studydetails/' + item.id, {
                state: item
              })}>
              <div className='auiz_left_total'>
                    <div className='auiz_header'>
                        <div className='auiz_left_container'>
                          <span>{item.title}</span>
                          <span className="auiz_left_container_span">|</span>
                          <span>{times(item)}天前</span>  
                        </div>
                    </div>
                    <main className='auiz_main'> 
                      <div className='auiz_left_image'>
                        <img src={item.cover} alt="" />
                      </div>
                      <div className='auiz_left_icon'>
                        <EyeOutlined /><span>{item.views}</span>
                        <span>·</span>
                        <ShareAltOutlined/><span>Share</span>
                      </div>
                    </main>
              </div>
          </div>
       
          })
        }
      </div>
    )
}

export default QuizPublic
