import { useState,useEffect,useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom'
// import './index.css'

export interface ArticleResponseTypeItemFC{
		id: string;
		title: string;
		cover: string|null;
		summary: string|null;
		content: string;
		html: string;
		toc: string;
		status:string;
		views:number;
		likes: number;
		isRecommended:boolean;
		needPassword: boolean;
		totalAmount:number|null;
		isPay: boolean;
		isCommentable:boolean;
		publishAt: string;
		createAt: string;
		updateAt: string;
		tags: Array<{
			id: string;
			label: string;
			value: string;
			createAt: string;
			updateAt:string;
		}>;
		[key:string]:any;
}
function Article(props:{itemValue:ArticleResponseTypeItemFC;[key:string]:any}) {
  
  return (
    <div className="article_container">
		<a href={"/article/"+props.itemValue.id} className="article_container_a">
			<div className="article_container_a_header">
				<span className="article_container_a_header_title">{props.itemValue.title}</span>
				<span className="article_container_a_header_line"></span>
				{/* <span className="article_container_a_header_text">{formatDate(props.itemValue.publishAt)}</span> */}
				<span className="article_container_a_header_line"></span>
				<span className="article_container_a_header_text">{props.itemValue.status}</span>
			</div>
			<div className="article_container_a_main">
				<span className="article_container_a_header_icon">
					<svg   viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2568" width="12" height="12"><path d="M480.2 921.5c-11.2 0-22.4-2.8-32.4-8.6C342.5 852.4 34.1 605.1 25.3 385.5c-3-76.7 24.4-149.4 77.4-204.5 50.5-52.6 120-82.8 190.6-82.8 69.9 0 136 28.6 186.3 80.6 0.5 0.6 1.2 0.7 1.7 0.7s1.2-0.1 1.7-0.7c50.2-51.9 116.4-80.6 186.2-80.6 77.7 0 155.4 36 208 96.2 48.1 55.1 69.4 123.6 59.9 192.8-30.6 223.6-303 454.1-422.5 524.9-10.6 6.3-22.5 9.4-34.4 9.4zM293.3 133.1c-61.1 0-121.5 26.3-165.5 72.1-46.4 48.2-70.4 111.8-67.7 179 7.7 193.8 289 431.9 405.1 498.6 9.6 5.5 21.7 5.3 31.7-0.6 128-75.8 378.5-300.2 405.8-499.6 8.1-59-10.3-117.7-51.7-165.1-46-52.8-114-84.3-181.7-84.3-60.3 0-117.6 24.8-161.2 69.9-7 7.2-16.7 11.3-26.7 11.3-10.2 0-19.7-4-26.7-11.3-43.8-45.2-101-70-161.4-70z" fill="#515151" p-id="2569"></path></svg>
				</span>
				<span className="article_container_a_header_text">{props.itemValue.likes}</span>
				<span className="article_a_point"></span>
				<span className="article_container_a_header_icon">
				<svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3654" width="12" height="12"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3-7.7 16.2-7.7 35.2 0 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766z" p-id="3655"></path><path d="M508 336c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176z m0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z" p-id="3656"></path></svg>
				</span>
				<span className="article_container_a_header_text">{props.itemValue.views}</span>
				<span className="article_a_point"></span>
				<span className="article_container_a_header_icon"><svg  viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4618" width="12" height="12"><path d="M768.704 703.616c-35.648 0-67.904 14.72-91.136 38.304l-309.152-171.712c9.056-17.568 14.688-37.184 14.688-58.272 0-12.576-2.368-24.48-5.76-35.936l304.608-189.152c22.688 20.416 52.384 33.184 85.216 33.184 70.592 0 128-57.408 128-128s-57.408-128-128-128-128 57.408-128 128c0 14.56 2.976 28.352 7.456 41.408l-301.824 187.392c-23.136-22.784-54.784-36.928-89.728-36.928-70.592 0-128 57.408-128 128 0 70.592 57.408 128 128 128 25.664 0 49.504-7.744 69.568-20.8l321.216 178.4c-3.04 10.944-5.184 22.208-5.184 34.08 0 70.592 57.408 128 128 128s128-57.408 128-128S839.328 703.616 768.704 703.616zM767.2 128.032c35.296 0 64 28.704 64 64s-28.704 64-64 64-64-28.704-64-64S731.904 128.032 767.2 128.032zM191.136 511.936c0-35.296 28.704-64 64-64s64 28.704 64 64c0 35.296-28.704 64-64 64S191.136 547.232 191.136 511.936zM768.704 895.616c-35.296 0-64-28.704-64-64s28.704-64 64-64 64 28.704 64 64S804 895.616 768.704 895.616z" p-id="4619" fill="#8a8a8a"></path></svg></span>
				<span className="article_container_a_header_text">分享</span>
			</div>
		</a>
    </div>
  )
}

export default Article