import React,{ useEffect, useState } from 'react'
import { useSelector, useDispatch } from "react-redux"
import { RecommendList } from "../../api/api"
import { RouterDispatch, RouterStore, ItemList } from "../../utils/types"
import '../../views/Index.css'

interface Props {
    
}

const Recommend = (props: Props) => {
    const dispatch:RouterDispatch = useDispatch()

    const RecommendList2 = useSelector((state:RouterStore) => state.reducer.RecommendList)

    const times = (stime:any) => {
        const tim = Math.round(
            (new Date().getTime() - new Date(stime.publishAt).getTime()) / 
            1000 / 
            60 / 
            60 / 
            24 
        )
        return tim
    }

    // let [toTopFlag,setToTopFlag] = useState(true)
    
    useEffect(()=>{
        dispatch(RecommendList())
        // const toggle = (e:any)=>{
        //     let distance = e.target.scrollTop
        //     if(distance < 300){
        //       setToTopFlag(false)
        //     }else{
        //       setToTopFlag(true)
        //     }
        //   }
        //   document.body.addEventListener('scroll',toggle)
        //   return ()=>{
        //     document.body.removeEventListener('scroll',toggle)
        //   }
    },[dispatch])
    return (
        <div className="auiz_right_top">
            <div className='auiz_right_top_span' style={{fontWeight:'600'}}>
                推荐阅读
            </div>
            <div className="auiz_right_top_rem">
                <ul>
                    {
                        RecommendList2?.map((item:ItemList,index:number)=>{
                            return <li key={index}>
                                <a>
                                    <span>{item.title}</span>
                                    <span>{times(item)}天前</span>
                                </a>
                            </li>
                        })
                    }
                </ul>
            </div>
        </div>
    )
}

export default Recommend
