import React,{ useEffect, useMemo, useState } from 'react'
import { useLocation } from 'react-router-dom'
// import './index.css'
import { useSelector, useDispatch } from "react-redux"
import { RouterDispatch, RouterStore, ClassList } from "../../utils/types"
import { Tag_List, Classify_List } from '../../api/api'
// import axios from '../../../../utils/request'
import '../../views/index/Article/style/index.css'
//@ts-ignore
import { RouteItem } from '@/types/types'
import { NavLink, Outlet ,useNavigate} from 'react-router-dom'
import Recommend from '../../components/Quiz/Recommend'
interface Props {
    
}

const ComTag = (props: Props) => {
    const history = useNavigate()

    const dispatch:RouterDispatch = useDispatch()

    const TagList = useSelector((state:RouterStore) => state.reducer.TagList)
    
    const Tag = (item:any) => {
        history(`/index/tag/${item.value}`, { state: item })
    }

    useEffect(()=>{
        dispatch(Tag_List())
    },[dispatch])
    return (
        <div className="auiz_right">
            <Recommend></Recommend>
            <div className="auiz_right_top">
                <div className='auiz_right_top_span' style={{fontWeight:'600'}}>
                    文章标签
                </div>
                <div className="auiz_right_top_rem">
                    <ul>
                        {
                            TagList?.map((item:ClassList,index:number)=>{
                            return <a key={index} onClick={()=>Tag(item)} className="article_container_a_tag_container" >
                                {item.label}
                            </a>
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default ComTag
