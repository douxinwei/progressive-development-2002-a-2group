import {useEffect} from 'react'
import '../../views/index/Quiz/Quiz.css'
import {useSelector,useDispatch} from "react-redux"
import { Classify_List} from "../../api/api"
import {RouterDispatch,RouterStore} from "../../utils/types"
import { useNavigate } from 'react-router-dom'
import '../../views/Index.css'

interface Props {
    
}

const Classify = (props: Props) => {
    const navigate = useNavigate()
    const dispatch:RouterDispatch = useDispatch()

    const ClassifyList = useSelector((state:RouterStore) => state.reducer.ClassifyList)

    useEffect(()=>{
        dispatch(Classify_List())
      },[dispatch])
    return (
        <div className="auiz_right_bottom">
            <div className='auiz_right_top_span' style={{fontWeight:'600'}}>
                文章分类
            </div>
            <div className="auiz_right_top_rem">
                <ul>
                    {
                        ClassifyList?.map((item:any,index:number)=>{
                            return <li key={index} onClick={()=>navigate('/index/quiz/'+item.value , {
                              state:item
                            })}>
                                <a>
                                    <span>{item.label}</span>
                                    <span>共计{item.articleCount}篇文章</span>
                                </a>
                            </li>
                        })
                    }
                </ul>
            </div>
        </div>
    )
}

export default Classify
