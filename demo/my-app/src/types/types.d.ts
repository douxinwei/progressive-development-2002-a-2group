import { JSXElement } from 'react'
import store from '../store/index'
// 路由列表
export type RouteList = Array<RouteItem>
export type RootDispatch = typeof store.dispatch

// 路由项
export type RouteItem = {
    path: string,
    redirect?: string,
    component?: JSXElement,
    children?: Array<RouteItem>,
    title?:string
}


// 文章类型
export type DataItem={
    id:number,
    title:string
    publishAt:string,
    src:string,
    likes:string,
    isRecommended:number
    views:number
    summary:string
    month:string
    cover:string
}