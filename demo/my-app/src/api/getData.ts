import axios from "axios";
import {Dispatch} from "redux"

export let get_list = ()=>{
    return async (dispatch:Dispatch)=>{
        let {status,data} = await axios.get('/api/articlelist')
        status == 200 && dispatch({
            type:"GETLIST",
            payload:data
        })
    }
}