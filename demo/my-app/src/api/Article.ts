import { Dispatch } from "redux"
import request from '../utils/request'

export const getArticleList = () => {
    return async (dispatch: Dispatch) => {
     
     const data = await request.get('/api/articlelist')
             dispatch({
                type: 'GET_ARTICLE_LIST',
                payload: data.data
            })

    }
}

export const getSweiperList = () => {
    return async (dispatch: Dispatch) => {
        
     const data = await request.get('/api/swiperlist')
             dispatch({
                type: 'GET_SWEIPER_LIST',
                payload: data.data
            })

    }
}