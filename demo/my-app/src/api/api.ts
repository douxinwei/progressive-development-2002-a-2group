import axios from "axios"
import { Dispatch } from "redux"
// import { _get_list } from "./axios"

export const getStudyList=()=>{
    return async  (dispatch:Dispatch)=>{
            axios.get('https://bjwz.bwie.com/wipi/api/knowledge?page=1&pageSize=12&status=publish').then((res)=>{
            dispatch({
                type:"GET_STUDY_LIST",
                payload:res.data.data
            })
            // let res = await _get_list()
            // dispatch({
            //     type:"GET_RECOMMEND_LIST",
            //     payload:res.data.data
            // })
        })
    }
}

export const RecommendList = () => {
    return  async (dispatch:Dispatch) => {
        axios.get('https://bjwz.bwie.com/wipi/api/article').then((res)=>{
            dispatch({
                type:"GET_RECOMMEND_LIST",
                payload:res.data.data
            })
          })
        //@ts-ignore
        // let res = await _get_list()
        // dispatch({
        //     type:"GET_RECOMMEND_LIST",
        //     payload:res.data.data
        // })
    }
}

export const Classify_List = () => {
    return (dispatch:Dispatch) => {
        axios.get('https://bjwz.bwie.com/wipi/api/category?articleStatus=publish').then((res)=>{
            dispatch({
                type:"GET_CLASSIFY_LIST",
                payload:res.data.data
            })
          })
    }
}

export const Tag_List = () => {
    return (dispatch:Dispatch) => {
        axios.get('https://bjwz.bwie.com/wipi/api/tag?articleStatus=publish').then((res)=>{
            dispatch({
                type:"Tag_List",
                payload:res.data.data
            })
        })
    }
}