import myAxios from "../utils/request"

// 文章详情
export let get_details =(id:string)=> myAxios.post(`/api/article/${id}`)

// 所有文章
export let get_AllList =()=> myAxios.post(`/api/articlelist`)

// 新增评论

export let postComment =(obj:any)=> myAxios.post(`/api/addcomment`,{
    ...obj
}) 


// 获取评论
export let getComment = (id:string)=>myAxios.get(`/api/getcomment`,{params:{articleId:id}})


/*通过code获取用户信息 */
export function getUserInfo(code:any){
    return myAxios.get('/api/getuserinfo?code='+code)
}



// 获取用户信息
export function isLoginStatus(id:string){
    return myAxios.get('/api/isloginstatus?id='+id)
}

// 点赞
export function addLikes(articleId:string,value:number){
    return myAxios.get('/api/addlikes?articleId='+articleId+'&value='+value)
}

// 获取点赞数
export function getLikes(articleId:string){
    return myAxios.get('/api/getlikes?articleId='+articleId)
}

// 客户端信息
export function getViw(){
    return myAxios.get('/api/view')
}