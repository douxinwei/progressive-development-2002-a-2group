import {legacy_createStore,applyMiddleware,combineReducers} from "redux"

import reducer from './modules/reducer'
import ArticleReducer from './modules/ArticleReducer'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

// 合并reducer
const allRed = combineReducers({
    reducer,
    ArticleReducer
})


export default legacy_createStore(allRed, applyMiddleware(thunk, logger))
