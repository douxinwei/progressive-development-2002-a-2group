import Cookies from "js-cookie"

const initialState = {
    data:[],
    language:Cookies.get('lan') || "zh",
    zh:["文章","归档","知识小册","发表文章"],
    en:["article","file","Booklet","publish"],
    skin:Cookies.get('skin')?true:false,
    articleId:'',
    userinfo:{
        state:false,
        username:''

    },

    // 知识小测
    StudyList:[],
    RecommendList:[],
    ClassifyList:[],
    //标签
    TagList:[]
}

const reducer = (state = initialState, { type, payload }: any) => {
    let newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        // 双语切换
        case "CHANGELAUGUAGE":
            newState.language = payload
           Cookies.set('lan',payload)
            return newState
        // 知识小测
        case "GET_STUDY_LIST":
            newState.StudyList=payload[0]
            return newState
        case 'GET_RECOMMEND_LIST':
            newState.RecommendList=payload[0]
            return newState
        case 'GET_CLASSIFY_LIST':
            newState.ClassifyList=payload
            return newState
        case "GETLIST":
            newState.data = payload
            return newState
        // 标签
        case 'Tag_List':
            newState.TagList = payload
            return newState
        case "changeUserInfo":
            newState.userinfo.state = payload
            return newState
        case "CHANGEID":
            newState.articleId=payload
        default:
            return newState
    }
}

export default  reducer
