
const initialState = {
       "SuggestedReadingList":[], //推荐阅读
       "swiperlist":[] //轮播图
}

const reducer = (state = initialState, { type, payload }: any) => {
    let newState = JSON.parse(JSON.stringify(state))
    switch (type) {
        case "GET_ARTICLE_LIST":
             newState.SuggestedReadingList = payload
            return newState
            
            case "GET_SWEIPER_LIST":
                newState.swiperlist = payload
               return newState
        default:
            return newState
    }
}

export default  reducer
