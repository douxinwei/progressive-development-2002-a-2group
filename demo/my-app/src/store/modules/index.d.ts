import store from ".."

export type StoreType={
    reducer:StoreItem
}

export type StoreItem={
    data:Array<DataItem>,
    zh:string[],
    en:string[],
    language:string,
    userinfo:UserInfoType,
    articleId:string,
    [key:string]:any,
}

export type UserInfoType={
    userinfo:{
        state:boolean,
        username:string
    }
}

// 文章类型
export type DataItem={
    id:number,
    title:string
    publishAt:string,
    src:string,
    likes:string,
    isRecommended:number
    views:number
    summary:string
    month:string
    cover:string
}

export type AppDispatch = typeof store.dispatch