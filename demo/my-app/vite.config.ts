import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  // 配置跨域
  server:{
    open:true,
    host:'0.0.0.0',
    proxy:{
      '/api':{
        target:"http://localhost:8090",
        changeOrigin:true,
        rewrite:(path)=>path.replace(/^\/api/,'')
      }
    }
  },
//12
   // 配置别名
   resolve: {
    // 配置别名
    alias: {
      '@': path.join(__dirname, 'src'),
      '@components': path.join(__dirname, './src/components'),
      '@utils': path.join(__dirname, './src/utils')
    }
  },
})
